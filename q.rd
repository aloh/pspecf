<resource schema="pspecf">
  <meta name="creationDate">2019-04-04T10:54:33Z</meta>

  <meta name="title">Planetary Spectrophotometer Facility</meta>
  <meta name="description">
    %this should be a paragraph or two (take care to mention salient terms)%
  </meta>
  <!-- Take keywords from 
    http://astrothesaurus.org/thesaurus
    if at all possible -->
  <meta name="subject">%keywords; repeat the element as needed%</meta>

  <meta name="creator">Cloutis, E.</meta>
  <meta name="instrument">%telescope or detector or code used%</meta>
  <meta name="facility">%observatory/probe at which the data was taken%</meta>

  <meta name="source">%ideally, a bibcode%</meta>
  <meta name="contentLevel">Research</meta>
  <meta name="type">Archive</meta>

  <meta name="coverage.waveband">Optical</meta>
  <meta name="coverage.waveband">Infrared</meta>

  <table id="epn_core" onDisk="True" adql="True">
    <publish sets="local,ivo_managed"/>

    <meta name="_associatedDatalinkService">
      <meta name="serviceId">dl</meta>
      <meta name="idColumn">granule_uid</meta>
    </meta>

    <mixin
      spatial_frame_type="none"
      optional_columns="access_url access_format access_estsize
        thumbnail_url file_name species 
        bib_reference publisher"
      >//epntap2#table-2_0</mixin>

    <mixin>//epntap2#localfile-2_0</mixin>
  </table>

  <data id="import">
    <property key="previewDir">previews</property>

    <sources pattern="data/*.xls" recurse="True"/>

    <customGrammar module="res/xlsreader">
      <rowfilter procDef="//products#define">
        <bind name="table">"\schema.epn_core"</bind>
        <bind name="accref">"{}!{}!{}".format(\inputRelativePath,
          @sheet_index, @column_index)</bind>
        <bind name="mime">"application/x-votable+xml"</bind>
        <bind name="path"
          >"\getConfig{web}{serverURL}/pspecf/q/get/qp/{}!{}!{}".format(
            \inputRelativePath, @sheet_index, @column_index)</bind>
        <bind name="preview">\splitPreviewPath{.png}</bind>
       </rowfilter>
    </customGrammar>

    <make table="epn_core">
      <rowmaker idmaps="*">
        <!-- in the following, you can write python expressions (e.g., strings
          are in single or double quotes), and/or pull things from the
          records your grammar spits out with @key

          Just delete bind elements you can't fill.
          
          Refer to http://docs.g-vo.org/DaCHS/ref.html#epntap2-populate-2-0
          for more information on the parameters.
          -->

        <var key="obsid">@prodtblAccref.split("/", 2)[-1]</var>

        <apply procDef="//epntap2#populate-2_0" name="fillepn">
          <bind key="granule_gid">\inputRelativePath</bind>
          <bind key="granule_uid">@obsid</bind>
          <bind key="obs_id">@obsid</bind>
          <bind key="instrument_host_name">"see http://www.minorplanetcenter.net/iau/lists/ObsCodesF.html or http://nssdc.gsfc.nasa.gov/nmc/ for terms to use here"</bind>
          <bind key="processing_level">3</bind>
          <bind key="target_name">'solid'</bind>
<!--          <bind key="creation_date">%Date of first entry of this granule%</bind>
          <bind key="dataproduct_type">%'im' for image, 'sp' for spectrum; see http://dc.g-vo.org/tableinfo/titan.epn_core#note-et_prod for what values are defined here.%</bind>
          <bind key="emergence_max">%Emergence angle during data acquisition, upper limit%</bind>
          <bind key="emergence_min">%Emergence angle during data acquisition, lower limit.%</bind>
          <bind key="incidence_max">%Incidence angle (solar zenithal angle) during data acquisition, upper limit%</bind>
          <bind key="incidence_min">%Incidence angle (solar zenithal angle) during data acquisition, lower limit.%</bind>
          <bind key="instrument_name">%use complete name + usual acronym, as in 'VISIBLE AND INFRARED THERMAL IMAGING SPECTROMETER VIRTIS'%</bind>
          <bind key="measurement_type">%UCD(s) defining the data, with multiple entries separated by hash (#) characters.%</bind>
          <bind key="modification_date">%Date of last modification (used to handle mirroring)%</bind>
          <bind key="obs_id">%Should be identical for granules resulting from the same observation; put in the file name if you have nothing else.%</bind>
          <bind key="phase_max">%Phase angle during data acquisition, upper limit%</bind>
          <bind key="phase_min">%Phase angle during data acquisition, lower limit.%</bind>
          <bind key="release_date">%Start of public access period%</bind>
          <bind key="s_region">%A spatial footprint of a dataset located on a spherical coordinate system%</bind>
          <bind key="service_title">%An acronym helpful when combining rows from multiple services%</bind>
          <bind key="spectral_range_max">%Spectral range, upper limit, in Hz%</bind>
          <bind key="spectral_range_min">%Spectral range, lower limit, in Hz%</bind>
          <bind key="spectral_resolution_max">%Spectral resolution, upper limit, in Hz%</bind>
          <bind key="spectral_resolution_min">%Spectral resolution, lower limit, in Hz%</bind>
          <bind key="spectral_sampling_step_max">%Spectral sampling step, upper limit, in Hz%</bind>
          <bind key="spectral_sampling_step_min">%Spectral sampling step, lower limit, in Hz%</bind>
          <bind key="target_class">%Choose from asteroid, dwarf_planet, planet, satellite, comet, exoplanet, interplanetary_medium, ring, sample, sky, spacecraft, spacejunk, star, UNKNOWN%</bind>
          <bind key="target_region">%Substructure observed (e.g., Atmosphere,
          Surface). Take terms from them Spase dictionary at
          http://www.spase-group.org/docs/dictionary%</bind> 

          <bind key="time_exp_max">%Integration time of the measurement, upper limit, in s%</bind>
          <bind key="time_exp_min">%Integration time of the measurement, lower limit, in s%</bind>
          <bind key="time_max">%Acquisition stop time (in JD)%</bind>
          <bind key="time_min">%Acquisition start time (in JD)%</bind>
          <bind key="time_sampling_step_max">%Sampling time for measurements of dynamical phenomena, upper limit, in s%</bind>
          <bind key="time_sampling_step_min">%Sampling time for measurements of dynamical phenomena, lower limit, in s%</bind>
          <bind key="time_scale">%Time scale used for the various times. Choose from TT, TDB, TOG, TOB, TAI, UTC, GPS, UNKNOWN%</bind>
          <bind key="time_scale">%Reference position used for the various
          times. The standard is a bit vague at this time. Remove this if
          unsure.%</bind> -->
        </apply>

        <apply procDef="//epntap2#populate-localfile-2_0"/>

        <!-- fill your custom columns with elements like
          <map dest="my_col_name">"foo"+@bar+"quux"</map>
        -->
      </rowmaker>
    </make>
  </data>				


  <table id="instance">
    <param name="associated_excel_file" type="text"
      description="I won't even write this"/>
    
    <column name="spectral" type="double precision"
      unit="nm" ucd="em.wl"
      tablehead="Spectral points"
      description="Wavelenghth XXX TODO: atmosphere?"/>
    <column name="flux" type="real"
      ucd="phot.flux.density;em.wl"
      tablehead="Fluxes"
      description="Fluxes XXX TODO: What sort?"/>
  </table>

  
  <data id="build_spectrum" auto="False">
    <embeddedGrammar>
      <iterator>
        <code>
          data = self.sourceToken["data"]
          for wl, flux in zip(data["spectral"], data["flux"]):
            if flux is not None:
              yield {"spectral": wl, "flux": flux}
        </code>
      </iterator>

      <pargetter>
        <setup>
          <code>
            import os
            from gavo import api
            xslreader, _ = api.loadPythonModule(
              rd.getAbsPath("res/xlsreader"))
          </code>
        </setup>
        <code>
          filename, sheet_index, column_index \
            = self.sourceToken["accref"].split("!", 2)
          filename = os.path.join(api.getConfig("inputsDir"), filename)
          spects = xslreader.SpectralCollection(filename)

          # preserve the parsed spectrum in the sourceToken for the iterator
          self.sourceToken["data"] = spects.extract_spectrum(
            int(sheet_index), int(column_index))
          self.sourceToken["data"]["filename"] = getInputsRelativePath(
            filename)
          self.sourceToken["data"]["sheet_index"] = sheet_index
          self.sourceToken["data"]["column_index"] = column_index
          
          return self.sourceToken["data"]
        </code>
      </pargetter>
    </embeddedGrammar>

    <make table="instance">
      <parmaker>
        <!-- in the following, we need to snip off "pspecf/data/" from
          filename because that's where the static renderer on the dl
          service points to. -->
        <map key="associated_excel_file"
          >("\getConfig{web}{serverURL}/\rdId/dl/static/"
            +@filename.split("/", 2)[-1])</map>
      </parmaker>
    </make>
  </data>

  <service id="get" allowed="qp">
    <meta name="title">Spectrum Formatter</meta>
    <meta name="description">
      This service formats our spectra as 
      spectral data model-compliant VOTables.
    </meta>
    <property name="queryField">accref</property>

    <pythonCore>
      <inputTable>
        <inputKey name="accref" type="text" description="The accref of
          the spectrum to format"/>
      </inputTable>
      <outputTable/>
      <coreProc>
        <setup>
          <code>
            from gavo import api
            from gavo.protocols import sdm
          </code>
        </setup>
        <code>
          dd = service.rd.getById("build_spectrum")
          spec_data = api.makeData(dd,
            forceSource=inputTable.getParamDict())
          return sdm.formatSDMData(spec_data,
            "application/x-votable+xml")
        </code>
      </coreProc>
    </pythonCore>

  </service>

  <service id="dl" allowed="dlmeta, static">
    <property name="staticData">data</property>

    <meta name="title">Pspecf datalink</meta>
    <meta name="description">
      A datalink service offering access to parts of the product's provenance
      chains.
    </meta>
    <datalinkCore>
      <descriptorGenerator>
        <!-- we override this to accept the granule_uids rather than
        full-fledged ivoids or accrefs -->
        <code>
          return ProductDescriptor.fromAccref(
            pubDID,
            "\schema/data/"+pubDID)
        </code>
      </descriptorGenerator>
      <metaMaker>
        <code>
          # this yields the name of the Excel file this was extracted
          # from.  We could take this from the filename attribute, but
          # we'd have to do a DB query to do that, and so we compute the
          # link based on our construction and layout.
          xls_path = "data/"+descriptor.pubDID.split("!")[0]
          yield descriptor.makeLinkFromFile(
            xls_path,
            description="A spreadsheet containing this spectrum and many"
              " other related ones.",
            contentType="application/vnd.ms-excel",
            semantics="#science")
        </code>
      </metaMaker>
    </datalinkCore>
  </service>

  <regSuite title="pspecf regression">
    <regTest title="pspecf EPN-TAP serves some data">
      <url parSet="TAP" QUERY="SELECT * from pspecf.epn_core
          WHERE granule_uid='2_1_Master_Pyroxenes.xls!0!11'"
        >/tap/sync</url>
      <code>
        rec = self.getFirstVOTableRow()
        self.assertEqual(rec["obs_id"], '2_1_Master_Pyroxenes.xls!0!11')
        self.assertTrue(rec["access_url"].endswith(
          "/getproduct/pspecf/data/2_1_Master_Pyroxenes.xls%210%2111"))
      </code>
    </regTest>
    
    <regTest title="pspecf spectrum generation works.">
      <url>get/qp/pspecf/data/2_1_Master_Pyroxenes.xls%210%2111</url>
      <code>
        self.assertXpath("//v:PARAM[@name='associated_excel_file']", {
          "value":
            # the following path also part of the next test
            EqualingRE(".*/pspecf/q/dl/static/2_1_Master_Pyroxenes.xls")})
        data = self.getVOTableRows()
        self.assertEqual(len(data), 462)
        self.assertEqual(data[0]["spectral"], 305.0)
        self.assertAlmostEqual(data[0]["flux"], 0.07336299866437912)
      </code>
    </regTest>

    <regTest title="pspecf static excel link returns excel">
      <!-- this URL tested for in "spectrum generation works" -->
      <url>dl/static/2_1_Master_Pyroxenes.xls</url>
      <code>
        self.assertHeader("content-type", "application/vnd.ms-excel")
        self.assertHasStrings(b"Pk\\x9a")
      </code>
    </regTest>
  </regSuite>
</resource>

<!-- vi:et:sw=2:sta
-->
