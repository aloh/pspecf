#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
The upstream data come in excel files with a fairly comples structure,
essentially consisting of worksheets representing spectra with contant
metadata; each worksheet has a couple of header lines with metadata,
and below that wavelengths in the first column, and fluxes for
different spectra in all remaining columns.

This module contains code to cope with this mess.  It will work
as a custom grammar for the data, returning one record per
spectrum.  In this record, we give both metadata and data, the latter
in spectral and flux columns.
"""

import xlrd
import numpy as np
import os

from gavo.grammars.customgrammar import CustomRowIterator

from gavo import api

def write_csv(filename, meta, wavl, spec):
    with open(filename, 'w') as wf:
        mkeys = list(meta.keys())
        for i in range(len(mkeys)):
            wf.write('#{} : {}\n'.format(mkeys[i], meta[mkeys[i]]))
        wf.write('#Columns:Wavelength,Spectrum\n')
        for j in range(wavl.size):
            wf.write('{},{}\n'.format(wavl[j], spec[j]))
    return

def plot_thumbnail(filename, meta, wavl, spec):
    plt.figure(figsize=(2.2, 2.2))
    plt.plot(wave_len, spectrum)
    plt.title(metadata['Sample ID'])
    # plt.xlabel('Wavelength (nm)')
    # plt.ylabel('Spectrum')
    plt.tight_layout()
    plt.savefig(filename, dpi=80)
    plt.close('all')
    return


class SpectralCollection(object):
    """a representation of the spectral collection from an exel file.

    This is being constructed with a path to the excel file (which
    actually is part of the metadata, so no file-like object or so
    will do).
    """
    def __init__(self, in_name):
        self.filename = in_name
        self.worksheets = xlrd.open_workbook(in_name, on_demand=True)
        self._meta_by_sheet = {}

    def _get_meta_for_sheet(self, sheet_index):
        if sheet_index not in self._meta_by_sheet:
            meta_col = self.worksheets.sheet_by_index(
                sheet_index).col_values(0)
            
            keys, data_start, data_end = [], None, None
            spectral = []
            for index, val in enumerate(meta_col):
                if data_start is None:
                    # collecting keywords until we get a number
                    if not isinstance(val, (float, int)):
                        if val:
                            keys.append(val)
                        else:
                            keys.append("row{}".format(index))
                    else:
                        data_start = index

                else:
                    # collecting wavelengths
                    if isinstance(val, (float, int)):
                        spectral.append(val)
                    else:
                        data_end = index
                        break
 
            self._meta_by_sheet[sheet_index] = {
                'keys': keys,
                'spectral': spectral,
                'data_start': data_start,
                'data_end': data_end,
                'filename': self.filename,
                'sheet_index': sheet_index,
            }

        return self._meta_by_sheet[sheet_index]
    
    def extract_spectrum(self, sheet_index, column_index):
        if column_index in [0,1]:
            raise api.UnknownURI(
                "No usable spectra in the first two columns")
        sheet = self.worksheets.sheet_by_index(sheet_index)
        # sm as in sheet_metadata, that is.  Don't get ideas here
        sm = self._get_meta_for_sheet(sheet_index)

        try:
            column = sheet.col_values(column_index)
        except IndexError:
            raise api.UnknownURI(
                "No such spectrum: {}, {}, {}".format(
                    self.filename, sheet_index, column_index))

        # first len(keys) things in the column are spectrum metadata
        spec = dict(zip(sm["keys"], column))
        spec.update(sm)

        # the rest should be something like spectral points,
        # except there are empty strings that really are NULL
        # and there's sometimes arbitrary trailing junk.
        flux = []
        for v in sheet.col_values(column_index)[
                sm["data_start"]:sm["data_end"]]:
            if isinstance(v, basestring):
                flux.append(None)
            else:
                flux.append(v)
        spec["flux"] = flux
        spec["column_index"] = column_index
        return spec
            
    def iter_all_spectra(self):
        for sheet_index in range(self.worksheets.nsheets):
            sheet = self.worksheets.sheet_by_index(sheet_index)
            for column_index in range(2, sheet.ncols):
                spec = self.extract_spectrum(sheet_index, column_index)
                if set(spec["flux"])!=set([None]):
                    yield spec


class RowIterator(CustomRowIterator):
    def _iterRows(self):
        sc = SpectralCollection(self.sourceToken)
        for spec in sc.iter_all_spectra():
            yield spec


if __name__=="__main__":
    sc = SpectralCollection("../data/2_1_Master_Pyroxenes.xls")
    for si in range(2, 100):
        print sc.extract_spectrum(1, si)

# vi:sw=4:et:sta
